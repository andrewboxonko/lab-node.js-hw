const express = require('express')
const morgan = require('morgan')
const fs = require('fs')


const createFolder = () => {
    fs.access("files", (err => {
        if (err) {
            fs.mkdir('files', { recursive: true }, (err) => {
                if (err) throw err;
                console.log('Directory was created successfully')
            });
        }
    }))
}

const checkFilename = (filename) => {
    return /^\w+.(log|txt|json|yaml|js)$/.test(filename)
}

const app = express();
const accessLogStream = fs.createWriteStream('access.log', { flags: 'a' });

app.use(express.json());
app.use(morgan('tiny', {stream: accessLogStream}));


createFolder()

app.post('/api/files', (req, res) => {
    const filename = req.body.filename;
    const content = req.body.content;

    if (!content || !filename){
        res.status(400).json({"message": `Please specify '${!content ? 'content': 'filename'} parameter`})
    } else if (filename && content) {
        if (checkFilename(filename)){
            fs.writeFile(__dirname + '/files/' + filename, content, (err) => {
                if (err) {
                    console.log(err)
                    res.status(500).json({"message": "Server error"})
                } else {
                    res.status(200).json({"message": "File created successfully"})
                }
            })
            console.log(`File ${filename} created successfully`)
        } else {
            res.status(400).json({"message": "Incorrect file name"})
        }
    }
})

app.post('/api/modify/', (req, res) => {
    const filename = req.body.filename;
    const content = req.body.content;

    if (!content || !filename){
        res.status(400).json({"message": `Please specify '${!content ? 'content': 'filename'} parameter`})
    } else if (filename && content) {
        if (checkFilename(filename)){
            fs.appendFile(__dirname + '/files/' + filename, content, (err) => {
                if (err) {
                    res.status(500).json({"message": "Server error"})
                } else {
                    res.status(200).json({"message": "File was modified successfully"})
                }
            })
            console.log(`File ${filename} created successfully`)
        } else {
            res.status(400).json({"message": "Incorrect file name"})
        }
    }
})

app.get('/api/files', ((req, res) => {
    fs.readdir('files', (err, files) => {
        if (err){
            res.status(500).json({"message": "Client error"})
        } else{
            res.status(200).json({"message": "Success",
                                             "files": files})
        }
    })
}))

app.get('/api/files/:file', (req, res) => {
    const filename= req.params.file;
    if (checkFilename(filename)){
        fs.readFile('files/' + filename, "utf-8", (err, content) => {
            if (err){
                res.status(400).json({"message": `No file with ${filename} filename found`})
            } else {
                fs.stat("files/" + filename, ((error, stats) => {
                    if (err){
                        throw err;
                    } else {
                        let birthtime = stats.birthtime;
                        res.status(200).json({ "message": "Success",
                            "filename": filename,
                            "content": content,
                            "extension": filename.slice(filename.indexOf('.') + 1),
                            "uploadedDate": birthtime})
                    }
                }))
            }
        })
    } else {
        res.status(400).json({"message": "Incorrect file name"})
    }
})

app.delete('/api/files/:file', (req, res) => {
    const filename= req.params.file;
    if (checkFilename(filename)){
        fs.unlink('files/' + filename, (err) => {
            if (err){
                res.status(400).json({"message": `No file with ${filename} filename found`})
            } else {
                res.status(200).json({ "message": "File was deleted successfully"})
            }
        })
    }
})

app.listen(8080, () => {
    console.log('Server works at port 8080')
})
